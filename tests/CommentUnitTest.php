<?php

namespace App\Tests;

use App\Entity\Article;
use App\Entity\Comment;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
      $comment = new Comment();
      $datetime = new DateTime();
      $article = new Article();

      $comment->setAuthor('author')
              ->setEmail('true@test.com')
              ->setContent('content')
              ->setCreatedAt($datetime)
              ->setArticle($article);
      
      $this->assertTrue($comment->getAuthor() === 'author');
      $this->assertTrue($comment->getEmail() === 'true@test.com');
      $this->assertTrue($comment->getContent() === 'content');
      $this->assertTrue($comment->getCreatedAt() === $datetime);
      $this->assertTrue($comment->getArticle() === $article);
    }

    public function testIsFalse(): void
    {
      $comment = new Comment();
      $datetime = new DateTime();
      $article = new Article();

      $comment->setAuthor('author')
              ->setEmail('email')
              ->setContent('content')
              ->setCreatedAt(new DateTime())
              ->setArticle(new Article());
      
      $this->assertFalse($comment->getAuthor() === 'false');
      $this->assertFalse($comment->getEmail() === 'false@test.com');
      $this->assertFalse($comment->getContent() === 'false');
      $this->assertFalse($comment->getCreatedAt() === $datetime);
      $this->assertFalse($comment->getArticle() === $article);
    }

    public function testIsEmpty(): void
    {
      $comment = new Comment();
      
      $this->assertEmpty($comment->getAuthor());
      $this->assertEmpty($comment->getEmail());
      $this->assertEmpty($comment->getContent());
      $this->assertEmpty($comment->getCreatedAt());
      $this->assertEmpty($comment->getArticle());
      $this->assertEmpty($comment->getId());
    }
}
