<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ArticleFunctionalTest extends WebTestCase
{
    public function testShouldDisplayArticle(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/articles');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Tous les articles');
    }

    public function testShouldDisplayOneArticle() {
      $client = static::createClient();
      $crawler = $client->request('GET', '/articles/article-test');

      $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Article Test');
    }
}
