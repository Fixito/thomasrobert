<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryFunctionalTest extends WebTestCase
{
  public function testShouldDisplayCategory(): void
  {
      $client = static::createClient();
      $crawler = $client->request('GET', '/categories');

      $this->assertResponseIsSuccessful();
      $this->assertSelectorTextContains('h1', 'Catégories');
  }

  public function testShouldDisplayOneCategory() {
    $client = static::createClient();
    $crawler = $client->request('GET', '/categories/category-test');

    $this->assertResponseIsSuccessful();
      $this->assertSelectorTextContains('h1', 'category_test');
  }
}
