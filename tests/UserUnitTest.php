<?php

namespace App\Tests;

use App\Entity\Article;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
      $user = new User();

      $user->setEmail('true@test.com')
          ->setFirstname('prenom')
          ->setLastname('nom')
          ->setPassword('password')
          ->setPhoneNumber('0123456789')
          ->setRoles(['ROLE_TEST']);

      $this->assertTrue($user->getEmail() === 'true@test.com');
      $this->assertTrue($user->getUsername() === 'true@test.com');
      $this->assertTrue($user->getFirstname() === 'prenom');
      $this->assertTrue($user->getLastname() === 'nom');
      $this->assertTrue($user->getPassword() === 'password');
      $this->assertTrue($user->getPhoneNumber() === '0123456789');
      $this->assertTrue($user->getRoles() === ['ROLE_TEST', 'ROLE_USER']);
    }

    public function testIsFalse(): void
    {
      $user = new User();

      $user->setEmail('true@test.com')
          ->setFirstname('prenom')
          ->setLastname('nom')
          ->setPassword('password')
          ->setPhoneNumber('0123456789');

      $this->assertFalse($user->getEmail() === 'false@test.com');
      $this->assertFalse($user->getUsername() === 'false');
      $this->assertFalse($user->getFirstname() === 'false');
      $this->assertFalse($user->getLastname() === 'false');
      $this->assertFalse($user->getPassword() === 'false');
      $this->assertFalse($user->getPhoneNumber() === 'false');
    }

    public function testIsEmpty(): void
    {
      $user = new User();

      $this->assertEmpty($user->getEmail());
      $this->assertEmpty($user->getUsername());
      $this->assertEmpty($user->getFirstname());
      $this->assertEmpty($user->getLastname());
      $this->assertEmpty($user->getPassword());
      $this->assertEmpty($user->getPhoneNumber());
      $this->assertEmpty($user->getId());
    }

    public function testAddGetRemoveArticle() {
      $user = new User();
      $article = new Article();

      $this->assertEmpty($user->getArticles());
  
      $user->addArticle($article);
      $this->assertContains($article, $user->getArticles());
  
      $user->removeArticle($article);
      $this->assertEmpty($user->getArticles());
    }
}
