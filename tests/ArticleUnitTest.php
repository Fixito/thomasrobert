<?php

namespace App\Tests;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class ArticleUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
      $article = new Article();
      $datetime = new DateTime();
      $category = new Category();
      $user = new User();

      $article->setTitle('nom')
              ->setContent('content')
              ->setIsPublished(true)
              ->setPublicationDate($datetime)
              ->setLastUpdateDate($datetime)
              ->setSlug('slug')
              ->setPicture('picture')
              ->addCategory($category)
              ->setUser($user);


      $this->assertTrue($article->getTitle() === 'nom');
      $this->assertTrue($article->getContent() === 'content');
      $this->assertTrue($article->getIsPublished() === true);
      $this->assertTrue($article->getPublicationDate() === $datetime);
      $this->assertTrue($article->getLastUpdateDate() === $datetime);
      $this->assertTrue($article->getSlug() === 'slug');
      $this->assertTrue($article->getPicture() === 'picture');
      $this->assertContains($category, $article->getCategory());
      $this->assertTrue($article->getUser() === $user);
    }

    public function testIsFalse(): void
    {
      $article = new Article();
      $datetime = new DateTime();
      $category = new Category();
      $user = new User();

      $article->setTitle('nom')
              ->setContent('content')
              ->setIsPublished(true)
              ->setPublicationDate($datetime)
              ->setLastUpdateDate($datetime)
              ->setSlug('slug')
              ->setPicture('picture')
              ->addCategory($category)
              ->setUser($user);


      $this->assertFalse($article->getTitle() === 'false');
      $this->assertFalse($article->getContent() === 'false');
      $this->assertFalse($article->getIsPublished() === false);
      $this->assertFalse($article->getPublicationDate() === new DateTime());
      $this->assertFalse($article->getLastUpdateDate() === new DateTime());
      $this->assertFalse($article->getSlug() === 'false');
      $this->assertFalse($article->getPicture() === 'false');
      $this->assertNotContains(new Category(), $article->getCategory());
      $this->assertFalse($article->getUser() === new User());
    }

    public function testIsEmpty(): void
    {
      $article = new Article();

      $this->assertEmpty($article->getTitle());
      $this->assertEmpty($article->getContent());
      $this->assertEmpty($article->getIsPublished());
      $this->assertEmpty($article->getPublicationDate());
      $this->assertEmpty($article->getLastUpdateDate());
      $this->assertEmpty($article->getSlug());
      $this->assertEmpty($article->getPicture());
      $this->assertEmpty($article->getCategory());
      $this->assertEmpty($article->getUser());
      $this->assertEmpty($article->getId());
    }

    public function testAddGetRemoveComment() {
      $article = new Article();
      $comment = new Comment();

      $this->assertEmpty($article->getComments());

      $article->addComment($comment);
      $this->assertContains($comment, $article->getComments());

      $article->removeComment($comment);
      $this->assertEmpty($article->getComments());
    }

    public function testAddGetRemoveCategory() {
      $article = new Article();
      $category = new Category();

      $this->assertEmpty($article->getComments());

      $article->addCategory($category);
      $this->assertContains($category, $article->getCategory());

      $article->removeCategory($category);
      $this->assertEmpty($article->getCategory());
    }
}
