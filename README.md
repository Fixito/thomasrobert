# ThomasRobert

ThomasRobert est un blog réalisé dans le cadre de ma formation de développeur web & web mobile.
## Environnement de développement

### Pré-requis

  * PHP 7.4
  * Composer
  * Symfony CLI
  * Docker
  * Docker-compose
  * nodesjs et npm

  Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony) :
    ```bash
    symfony check:requirements
    ```
### Lancer l'environnement de développement

  ```bash
  composer install
  npm install
  npm run build
  docker-compose up -d
  symfony serve -d
  ```

### Ajouter des données de tests

    ```bash
  symfony console doctrine:fixtures:load
  ```
  
## Lancer des test

  ```bash
  php bin/phpunit --testdox
  ```

## Production

### Envoie des mails de Contacts

Les mails de prise de contact sont stockés en BDD, pour les envoyer au blogueur par mail, il faut mettre en place un cron sur :

```bash
  symfony console app:send-contact
  ```