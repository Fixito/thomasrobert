<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(
        Request $request,
        ArticleRepository $articleRepository,
        CategoryRepository $categoryRepository
    ): Response {
        $hostname = $request->getSchemeAndHttpHost();
        $urls = [];

        $urls[] = ['loc' => $this->generateUrl('home')];
        $urls[] = ['loc' => $this->generateUrl('categories')];
        $urls[] = ['loc' => $this->generateUrl('about')];
        $urls[] = ['loc' => $this->generateUrl('contact')];

        foreach ($articleRepository->findAll() as $article) {
            $urls[] = [
                'loc'     => $this->generateUrl('articles_details', ['slug' => $article->getSlug()]),
                'lastmod' => $article->getPublicationDate()->format('Y-m-d')
            ];
        }

        foreach ($categoryRepository->findAll() as $category) {
            $urls[] = [
                'loc' => $this->generateUrl('articles_categories', ['slug' => $category->getSlug()]),
            ];
        }

        $response = new Response(
            $this->renderView('sitemap/index.xml.twig', [
              'urls' => $urls,
              'hostname' => $hostname
            ]),
            200
        );

        $response->headers->set('Content-type', 'text/xml');

        return $response;
    }
}
