<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use App\Service\CommentService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/articles", name="articles")
     */
    public function index(
        ArticleRepository $articleRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $articleRepository->findBy([], ['id' => 'DESC']);

        $articles = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('article/articles.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/articles/{slug}", name="articles_details")
     */
    public function details(
        Article $article,
        Request $request,
        CommentService $commentService,
        CommentRepository $commentRepository
    ): Response {
        $comments = $commentRepository->findComments($article);
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
              $comment = $form->getData();
              $commentService->persistComment($comment, $article, null);

              return $this->redirectToRoute('articles_details', ['slug' => $article->getSlug()]);
        }

        return $this->render('article/details.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'comments' => $comments
        ]);
    }
}
