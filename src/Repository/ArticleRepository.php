<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @return Article[] returns an array of Article objects
     */
    public function lastThree()
    {
        return $this->createQueryBuilder('a')
          ->orderBy('a.id', 'DESC')
          ->setMaxResults(4)
          ->getQuery()
          ->getResult();
    }

    /**
     * @return Article[] returns an array of Article objects
     */
    public function findAllArticles(Category $category): array
    {
        return $this->createQueryBuilder('a')
          ->where(':category MEMBER of a.category')
          ->setParameter('category', $category)
          ->getQuery()
          ->getResult();
    }
}
