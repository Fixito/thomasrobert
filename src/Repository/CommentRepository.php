<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function findComments($value)
    {
        if ($value instanceof Article) {
            $object = 'article';
        }

        return $this->createQueryBuilder('c')
                    ->andWhere('c.' . $object . ' = :val')
                    ->andWhere('c.isPublished = true')
                    ->setParameter('val', $value->getId())
                    ->orderBy('c.id', 'DESC')
                    ->getQuery()
                    ->getResult();
    }
}
