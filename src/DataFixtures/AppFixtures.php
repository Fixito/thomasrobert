<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // Utilisation de Faker
        $faker = Factory::create('fr_FR');

        // Création d'un utilisateur
        $user = new User();

        $user->setEmail('user@test.com')
            ->setFirstname($faker->firstName())
            ->setLastname($faker->lastName())
            ->setPhoneNumber($faker->phoneNumber())
            ->setAbout($faker->paragraphs(3, true))
            ->setRoles(["ROLE_BLOGUEUR"]);

        $password = $this->encoder->encodePassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);

        // Création d'un article pour les test
        $article = new Article();

        $article->setTitle('Article Test')
                ->setPublicationDate($faker->dateTimeBetween('-6 month', 'now'))
                ->setContent($faker->text(350))
                ->setPicture('post-bg.jpg')
                ->setIsPublished(true)
                ->setSlug('article-test')
                ->setUser($user);
        
        $manager->persist($article);

        // Création de 5 catégories
        for ($i = 0; $i<5; $i++) {
            $category = new Category();

            $category->setName($faker->words(1, true))
                    ->setSlug($faker->slug(3))
                    ->setDescription($faker->words(10, true));

            $manager->persist($category);

            // Création de 2 articles
            $article = new Article();

            $article->setTitle($faker->words(3, true))
                    ->setPublicationDate($faker->dateTimeBetween('-6 month', 'now'))
                    ->setIsPublished(true)
                    ->setContent($faker->text(350))
                    ->setSlug($faker->slug(3))
                    ->setPicture('post-bg.jpg')
                    ->addCategory($category)
                    ->setUser($user);
            
            $manager->persist($article);

            $article = new Article();

            $article->setTitle($faker->words(3, true))
                    ->setPublicationDate($faker->dateTimeBetween('-6 month', 'now'))
                    ->setIsPublished(true)
                    ->setContent($faker->text(350))
                    ->setSlug($faker->slug(3))
                    ->setPicture('post-bg.jpg')
                    ->addCategory($category)
                    ->setUser($user);
            
            $manager->persist($article);
        }

        //Création d'une catégorie pour les tests
        $category = new Category();

        $category->setName('category_test')
                ->setSlug('category-test')
                ->setDescription($faker->words(10, true));
        $manager->persist($category);

        $manager->flush();
    }
}
