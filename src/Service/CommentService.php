<?php
namespace App\Service;

use App\Entity\Article;
use App\Entity\Comment;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }
    
    public function persistComment(
        Comment $comment,
        Article $article = null
    ) : void {
        $comment->setIsPublished(false)
                ->setArticle($article)
                ->setCreatedAt(new DateTime('now'));

        $this->manager->persist($comment);
        $this->manager->flush();
        $this->flash->add('success', 'Votre commentaire est bien envoyé, merci. Il sera publié après validation.');
    }
}
